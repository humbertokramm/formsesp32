#######################################
# Syntax Coloring Map For IRremote
#######################################

#######################################
# Datatypes (KEYWORD1)
#######################################

config	KEYWORD1
configForm	KEYWORD1
googleForms	KEYWORD1

#######################################
# Methods and Functions (KEYWORD2)
#######################################

addToQueue	KEYWORD2
processQueue	KEYWORD2
fields	KEYWORD2

#######################################
# Constants (LITERAL1)
#######################################

FIELDS	LITERAL1