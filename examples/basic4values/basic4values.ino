#include <WiFi.h>
#include "googleForms.h"

#include "secrets.h"


googleForms forms(10);

//Vars
const uint16_t sampleRate = 60*1000; //segundos


float var1 = 100;
float var2 = 200;
float var3 = 300;
float var4 = 400;


void handleValue(){
	static uint32_t temp;
	// Entra somente após um tempo
	if((temp<millis()-sampleRate)&&(millis()>sampleRate)){
		temp = millis();
		
		values[0] = String(var1++);
		values[1] = String(var2++);
		values[2] = String(var3++);
		values[3] = String(var4++);
		
		forms.addToQueue(values, sizeof(fields)/sizeof(String));
	}
}

void setup() {
	Serial.begin(115200);

	// Conecte-se à rede Wi-Fi
	WiFi.begin(ssid, password);

	while (WiFi.status() != WL_CONNECTED) {
		delay(1000);
		Serial.println("Conectando ao WiFi...");
	}
	Serial.println("Conectado ao WiFi");
	
	//Carrega os valores do formulário
	forms.config(fields,sizeof(fields)/sizeof(String));
	forms.configForm(myForm);
}

void loop() {
	//Trata as variáveis
	handleValue();

	// Realize o processamento da fila.
	forms.processQueue();
	
	// delay padrão
	delay(1);
}