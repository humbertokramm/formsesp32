#include "Queue2.h"

/*---------------------------------------------------*/
/* Constructor
/*---------------------------------------------------*/
Queue::Queue(int size) {
	front = 0;
	rear = -1;
	maxSize = size;
	currentSize = 0;
	items = new String*[maxSize];
	for (int i = 0; i < maxSize; i++) {
		items[i] = new String[size];
	}
}
/*---------------------------------------------------*/
Queue::~Queue() {
	for (int i = 0; i < maxSize; i++) {
		delete[] items[i];
	}
	delete[] items;
}
/*---------------------------------------------------*/
bool Queue::isEmpty() {
	return (currentSize == 0);
}
/*---------------------------------------------------*/
bool Queue::isFull() {
	return (currentSize == maxSize);
}
/*---------------------------------------------------*/
bool Queue::enqueue(String values[], int size) {

	if (isFull()) {
		// Fila cheia, não faça nada ou imprima uma mensagem de erro, se desejar.
		Serial.println("Fila cheia");
		return false;
	}
	else {
		rear = (rear + 1) % maxSize;
		for (int i = 0; i < size; i++) {
			items[rear][i] = values[i];
		}
		currentSize++;
	}
	return true;
}
/*---------------------------------------------------*/
void Queue::dequeue(String result[], int size) {
	if (isEmpty()) {
		// Fila vazia, não faça nada ou imprima uma mensagem de erro, se desejar.
		for (int i = 0; i < size; i++) {
			result[i] = "";
		}
	} else {
		for (int i = 0; i < size; i++) {
			result[i] = items[front][i];
		}
		front = (front + 1) % maxSize;
		currentSize--;
	}
}
/*---------------------------------------------------*/
bool Queue::frontValue(String result[], int size) {
	if (isEmpty()) {
		// Fila vazia, não faça nada ou imprima uma mensagem de erro, se desejar.
		return false;
	}
	else {
		for (int i = 0; i < size; i++) {
				result[i] = items[front][i];
		}
		return true;
	}
}
/*---------------------------------------------------*/