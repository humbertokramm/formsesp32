/**
 * Classe para comportar as funções de resposta do formulário.
 * Author: Humberto Kramm
 * Created on Mon Set 25 2023
 *
 * Copyright (c) 2023 - Humberto Kramm
 * 
 * @class googleForms
 * @constructor 
 * @extends Stage
*/
#include "googleForms.h"


/*---------------------------------------------------*/
/* Construtor
/*---------------------------------------------------*/
googleForms::googleForms(int queueSize)  : myQueue(queueSize) { }

/*---------------------------------------------------*/
/* Função para envios
/*---------------------------------------------------*/
bool googleForms::send(String * f){
	bool submitted = false;

	//Inicia a montagem da URL
	String url = URL_START+form+URL_END;
	
	//Adiciona os valores da fila na URL
	for(int i=0;i<Size;i++){
		url += "&entry."+filds[i]+"="+f[i];
	}

	//Responde o formulário
	http.begin(url);

	// Realize a solicitação HTTP GET
	int httpCode = http.GET();

	// Verifique se a solicitação foi bem-sucedida
	if (httpCode > 0) {
		Serial.printf("Código de status HTTP: %d\n", httpCode);
		//Pega e limpa a requisição HTTP
		String payload = http.getString();
		submitted =  true;
	}
	else {
		Serial.printf("Falha na solicitação HTTP, código de erro: %s\n", http.errorToString(httpCode).c_str());
	}
	// Libere os recursos
	http.end();
	return submitted;
}
/*---------------------------------------------------*/
void googleForms::config(String * f,int s){
	Size = s;
	for(int i=0;i<Size;i++){
		filds[i]=f[i];
	}
}
/*---------------------------------------------------*/
void googleForms::configForm(String f){
	form = f;
}
/*---------------------------------------------------*/
bool googleForms::addToQueue(String values[], int size) {
	return myQueue.enqueue(values, size);
}
/*---------------------------------------------------*/
void googleForms::processQueue() {
	//return;
	String result[Size];
	if (myQueue.frontValue(result, Size)) {
		// Processa e remova os valores da fila se transmitido.
		if(send(result)) myQueue.dequeue(result, Size);
	}
}
/*---------------------------------------------------*/