#ifndef QUEUE_2_H
#define QUEUE_2_H

#include <Arduino.h>

class Queue {
	private:
		String** items;  // Array de arrays de strings
		int front;
		int rear;
		int maxSize;     // Tamanho máximo dos arrays internos
		int currentSize; // Tamanho atual (número de elementos) da fila

	public:
		Queue(int size); // Construtor com tamanho máximo
		~Queue();        // Destrutor
		bool isEmpty();
		bool isFull();
		bool enqueue(String values[], int size);
		void dequeue(String result[], int size);
		bool frontValue(String result[], int size);
};
#endif