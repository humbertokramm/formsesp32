/**
 * Classe para comportar as funções de resposta do formulário.
 * Author: Humberto Kramm
 * Created on Mon Set 25 2023
 *
 * Copyright (c) 2023 - Humberto Kramm
 * 
 * @class googleForms
 * @constructor 
 * @extends Stage
*/

#ifndef GOOGLEFORMS_H
#define GOOGLEFORMS_H


#include <HTTPClient.h>

#include "Queue2.h"

/*---------------------------------------------------*/
//Definições
/*---------------------------------------------------*/
#define MAX_FIELDS		4
#define URL_START	"https://docs.google.com/forms/d/e/"
#define URL_END	"/formResponse?usp=pp_url&"

/*---------------------------------------------------*/
class googleForms// : public Queue
{
	public:
		googleForms(int queueSize);
		bool send(String * f);
		void config(String * f,int s);
		void configForm(String f);
		bool addToQueue(String values[], int size);
		void processQueue();
	private:
		// Crie uma instância do objeto HTTPClient
		HTTPClient http;
		int Size;
		String filds[MAX_FIELDS];
		String form = "MY_FORM";
		bool sendingData = false;
		Queue myQueue;
};
/*---------------------------------------------------*/
#endif // GOOGLEFORMS_H
